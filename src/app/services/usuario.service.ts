import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment as env } from "../environment/environment";

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  URL = env.endpointURL;

  constructor(private http: HttpClient) { }

  allUsuarios(){
    return this.http.get<any>(`${this.URL}usuarios/all/`)
  }

  crearUsuario(nombre:string, apellido:string, rut:string, direccion:string, celular:string){
    return this.http.post<any>(`${this.URL}usuarios/new`,{nombre:nombre, apellido:apellido,rut:rut,
      direccion:direccion, celular:celular});
  }


  modificarUsuario(nombre:string, apellido:string, rut:string, direccion:string, celular:string, idusuario:number){
    return this.http.put<any>(`${this.URL}usuarios/modificar`,{nombre:nombre, apellido:apellido,rut:rut,
      celular:celular, direccion:direccion, idusuario:idusuario});
  }

  eliminarUsuario(idusuario:number){
    return this.http.get<any>(`${this.URL}usuarios/eliminar/${idusuario}`);
  } 


}
