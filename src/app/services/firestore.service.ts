import { inject, Injectable } from '@angular/core';
import { collection, collectionData, Firestore, doc, docData, getDoc, setDoc, updateDoc, deleteDoc, DocumentReference } from '@angular/fire/firestore';
import { v4 as uuidv4 } from 'uuid';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FirestoreService {
  

  private firestore: Firestore = inject(Firestore);


  constructor() { }

  getCollectionChanges<tipo>(path:string){
    const refCollection = collection(this.firestore, path);
    return collectionData(refCollection) as Observable<tipo[]>;
  }

  createDocumentID(data: any, enlace: string, idDoc: string) {
    const document = doc(this.firestore, `${enlace}/${idDoc}`);
    return setDoc(document, data);
  }

  createIdDoc() {
    return uuidv4()
  }

  async updateDocument(data: any, enlace: string) {
    const document = doc(this.firestore, `${enlace}`);
    return updateDoc(document, data)
  }

  async updateDocumentID(data: any, enlace: string, idDoc: string) {
    const document = doc(this.firestore, `${enlace}/${idDoc}`);
    return updateDoc(document, data)
  }

  deleteDocumentID(enlace: string, idDoc: string) {
    const document = doc(this.firestore, `${enlace}/${idDoc}`);
    return deleteDoc(document);
  }


}
