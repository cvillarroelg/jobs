export interface Trabajador {
    nombre: string;
    apellido: string;
    rut: string;
    celular: string;
    trabajo: string;
}