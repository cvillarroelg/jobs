import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSidenav } from '@angular/material/sidenav';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DialogAgregarTrabajoComponent } from '../dialog-agregar-trabajo/dialog-agregar-trabajo.component';
import { Firestore, collectionData, collection } from '@angular/fire/firestore';
import { delay, finalize, Observable, retry } from 'rxjs';
import { inject } from '@angular/core';
import { CollectionReference } from 'firebase/firestore';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/compat/firestore';
import { FirestoreService } from '../services/firestore.service';
import { Categoria } from '../common/models/categorias.model';
import { DialogModificarTrabajoComponent } from '../dialog-modificar-trabajo/dialog-modificar-trabajo.component';
import Swal from 'sweetalert2';

interface Item {
  name: string,
};

@Component({
  selector: 'app-trabajos',
  templateUrl: './trabajos.component.html',
  styleUrls: ['./trabajos.component.scss']
})
export class TrabajosComponent implements OnInit {

  dataSourceTrabajos !: MatTableDataSource<any>;
  displayedColumns: string[] = ['Nombre', 'Accion'];

  //firestore
  // private itemsCollection: AngularFirestoreCollection<Item>;
  items!: Observable<Item[]>;
  firestore: Firestore = inject(Firestore);
  loading = false;

  @ViewChild(MatPaginator) paginator !: MatPaginator;
  @ViewChild(MatSort) sort !: MatSort;

  @ViewChild(MatSidenav)
  sidenav!: MatSidenav;

  categoria!: Categoria;
  categorias: Categoria[] = [];

  newUser!: Categoria;
  

  constructor(private dialog: MatDialog, private firestoreService: FirestoreService) { 
  }

  ngOnInit(): void {
    // console.log("items: ", this.items);
    this.cargarTrabajos();
    // console.log("datasource trabajos: ", this.dataSourceTrabajos);
  }

  cargarTrabajos() {
    this.loading = true;
    this.firestoreService.getCollectionChanges<Categoria>('categorias')
    .pipe(
      retry(1),
      delay(0),
      finalize(() => {
        this.loading = false;
      })
    )
    .subscribe( data => {
      if (data) {
        this.categorias = data;
        console.log("data: ", this.categorias);
        this.dataSourceTrabajos = new MatTableDataSource<Categoria>(this.categorias);
        this.dataSourceTrabajos.paginator = this.paginator;
        this.dataSourceTrabajos.sort = this.sort;
      }
      this.loading = false;
    })
    // this.dataSourceTrabajos = new MatTableDataSource<Categoria>(this.categorias);
    // console.log("datasource en cargarTrabajos: ", this.dataSourceTrabajos);

  }

  // initUser() {
  //   this.newUser = {
  //     nombre: '',
  //     id: this.firestoreService.createIdDoc()
  //   }
  // }

  // async save() {
  //   this.loading = true;
  //   await this.firestoreService.createDocumentID(this.newUser, 'Categorias', this.newUser.id)
  //   this.loading = false;
  // }

  agregarTrabajo(){
    console.log("Agregar trabajo");
    this.dialog.open(DialogAgregarTrabajoComponent,{
      width:'50%'
    })
  }

  editarTrabajo(data:any){
    this.dialog.open(DialogModificarTrabajoComponent,{
      width:'50%',
      data:data
    })
  }

  eliminarTrabajo(user:Categoria){
    Swal.fire({
      title: '¿Estás seguro que quieres eliminar el trabajo?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, eliminar trabajo',
      cancelButtonText: 'Cancelar',
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire('Eliminado!', 'El trabajo ha sido eliminado!', 'success').then(
          (result) => {
            this.delete(user);            
          }
        );
      }
    });

  }

  async delete(user: Categoria) {
    this.loading = true;
    await this.firestoreService.deleteDocumentID('categorias', user.id);
    this.loading = false;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceTrabajos.filter = filterValue.trim().toLowerCase();

    if (this.dataSourceTrabajos.paginator) {
      this.dataSourceTrabajos.paginator.firstPage();
    }
  }

}
