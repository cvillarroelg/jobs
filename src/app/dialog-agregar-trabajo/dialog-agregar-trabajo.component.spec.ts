import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogAgregarTrabajoComponent } from './dialog-agregar-trabajo.component';

describe('DialogAgregarTrabajoComponent', () => {
  let component: DialogAgregarTrabajoComponent;
  let fixture: ComponentFixture<DialogAgregarTrabajoComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DialogAgregarTrabajoComponent]
    });
    fixture = TestBed.createComponent(DialogAgregarTrabajoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
