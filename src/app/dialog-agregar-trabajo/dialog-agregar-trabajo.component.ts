import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { Categoria } from '../common/models/categorias.model';
import { FirestoreService } from '../services/firestore.service';

@Component({
  selector: 'app-dialog-agregar-trabajo',
  templateUrl: './dialog-agregar-trabajo.component.html',
  styleUrls: ['./dialog-agregar-trabajo.component.scss'],
})
export class DialogAgregarTrabajoComponent implements OnInit {
  Formulario!: FormGroup;

  newUser!: Categoria;

  loading = false;

  constructor(
    public dialogRef: MatDialogRef<DialogAgregarTrabajoComponent>,
    private formBuilder: FormBuilder,
    private firestoreService: FirestoreService,
    private router: Router
  ) {}

  ngOnInit(): void {
    // this.Formulario = this.formBuilder.group({
    //   nombre: ['',Validators.required],
    // });
    this.initUser();
  }

  initUser() {
    this.newUser = {
      nombre: '',
      id: this.firestoreService.createIdDoc(),
    };
  }

  async save() {
    this.loading = true;
    await this.firestoreService.createDocumentID(
      this.newUser,
      'categorias',
      this.newUser.id
    );
    this.loading = false;
    this.router.navigate(['trabajos']).then(() => {
      window.location.reload();
    });
  }

  edit(user: Categoria) {
    console.log('edit -> ', user);
    this.newUser = user;
  }

  onNoClick() {
    this.dialogRef.close();
  }

  agregarTrabajo() {
    Swal.fire({
      title: '¿Estás seguro que quieres agregar el trabajo?',
      // text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, agregar trabajo',
      cancelButtonText: 'Cancelar',
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire('Agregado!', 'El trabajo ha sido agregado!', 'success').then(
          (result) => {
            this.save();
            // let nombre = this.Formulario.get('nombre')?.value;
            // let apellido = this.Formulario.get('apellido')?.value;
            // let celular = this.Formulario.get('celular')?.value;
            // let rut = this.Formulario.get('rut')?.value;
            // let direccion = this.Formulario.get('direccion')?.value;
            //borrar
            // this._usuarioService.crearUsuario(nombre,apellido,rut,direccion,celular)
            // .subscribe((res:any) => {
            //   console.log("RES:",res);
            // });
            // this.router.navigate(['usuarios'])
            // .then(() => {
            //   window.location.reload();
            // });
          }
        );
      }
    });
  }
}
