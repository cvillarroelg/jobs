import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSidenav } from '@angular/material/sidenav';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { DialogAgregarUsuarioComponent } from '../dialog-agregar-usuario/dialog-agregar-usuario.component';
import { UsuarioService } from '../services/usuario.service';
import { DialogModificarUsuarioComponent } from '../dialog-modificar-usuario/dialog-modificar-usuario.component';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.scss']
})
export class UsuariosComponent implements OnInit {

  dataSourceClientes !: MatTableDataSource<any>;
  displayedColumns: string[] = ['Nombre', 'Apellido', 'Rut', 'Celular','Direccion', 'Accion'];

  @ViewChild(MatPaginator) paginator !: MatPaginator;
  @ViewChild(MatSort) sort !: MatSort;

  @ViewChild(MatSidenav)
  sidenav!: MatSidenav;

  constructor(private dialog: MatDialog, private _usuarioService: UsuarioService, private router: Router) { }

  ngOnInit(): void {
    this.getAllClientes();
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceClientes.filter = filterValue.trim().toLowerCase();

    if (this.dataSourceClientes.paginator) {
      this.dataSourceClientes.paginator.firstPage();
    }
  }

  agregarUsuario(){
    console.log("Agregar usuario");
    this.dialog.open(DialogAgregarUsuarioComponent,{
      width:'50%'
    })
  }

  editarUsuario(row:any){
    console.log("Modificar usuario");
    this.dialog.open(DialogModificarUsuarioComponent,{
      width:'50%',
      data:{
        idusuario: row.idusuario,
        nombre:row.nombre,
        apellido:row.apellido,
        celular: row.celular,
        direccion: row.direccion,
        rut: row.rut
      }
    })
  }

  getAllClientes(){
    this._usuarioService.allUsuarios().subscribe((usuarios:any) =>{
      console.log("res all usuarios:",usuarios);
      this.dataSourceClientes = new MatTableDataSource(usuarios);
      this.dataSourceClientes.paginator = this.paginator;
      this.dataSourceClientes.sort = this.sort;
      
    })
  }

  // eliminarUsuario(idusuario:number){
  //   this._usuarioService.eliminarUsuario(idusuario).subscribe((res:any) => {
  //     console.log("res: ", res);
  //   });
  // }

  eliminarUsuario(idusuario:number){
    Swal.fire({
      title: '¿Estás seguro que quieres eliminar al usuario?',
      // text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, eliminar usuario',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire(
          'Agregado!',
          'El usuario ha sido eliminado!',
          'success'
        ).then((result) => {
          //borrar
          this._usuarioService.eliminarUsuario(idusuario).subscribe((res:any) => {
            console.log("res: ", res);
          });
          this.router.navigate(['usuarios'])
          .then(() => {
            window.location.reload();
          });
        });
      }
    })

    

  }

}
