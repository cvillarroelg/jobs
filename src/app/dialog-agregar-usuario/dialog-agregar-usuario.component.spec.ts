import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogAgregarUsuarioComponent } from './dialog-agregar-usuario.component';

describe('DialogAgregarUsuarioComponent', () => {
  let component: DialogAgregarUsuarioComponent;
  let fixture: ComponentFixture<DialogAgregarUsuarioComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DialogAgregarUsuarioComponent]
    });
    fixture = TestBed.createComponent(DialogAgregarUsuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
