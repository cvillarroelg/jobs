import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { UsuarioService } from '../services/usuario.service';

@Component({
  selector: 'app-dialog-agregar-usuario',
  templateUrl: './dialog-agregar-usuario.component.html',
  styleUrls: ['./dialog-agregar-usuario.component.scss']
})
export class DialogAgregarUsuarioComponent implements OnInit {

  Formulario !: FormGroup;

  constructor(private formBuilder: FormBuilder, private router: Router, public dialogRef: MatDialogRef<DialogAgregarUsuarioComponent>,
    private _usuarioService: UsuarioService) { }

  ngOnInit(): void {
    this.Formulario = this.formBuilder.group({
      nombre: ['',Validators.required],
      apellido: ['',Validators.required],
      celular: ['',Validators.required],
      rut:['',Validators.required],
      direccion:['',Validators.required]
    });
  }

  onNoClick(){
    this.dialogRef.close();
  }

  // agregarUsuario(){
  //   console.log("agregar usuario");
  // }

  getAllUsuarios(){
    console.log("all usuarios:");
  }

  agregarUsuario(){
    Swal.fire({
      title: '¿Estás seguro que quieres agregar al usuario?',
      // text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, agregar usuario',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire(
          'Agregado!',
          'El usuario ha sido agregado!',
          'success'
        ).then((result) => {
          let nombre = this.Formulario.get('nombre')?.value;
          let apellido = this.Formulario.get('apellido')?.value;
          let celular = this.Formulario.get('celular')?.value;
          let rut = this.Formulario.get('rut')?.value;
          let direccion = this.Formulario.get('direccion')?.value;
          //borrar
          this._usuarioService.crearUsuario(nombre,apellido,rut,direccion,celular)
          .subscribe((res:any) => {
            console.log("RES:",res);
          });
          this.router.navigate(['usuarios'])
          .then(() => {
            window.location.reload();
          });
        });
      }
    })

  }

}
