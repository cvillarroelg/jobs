import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogModificarTrabajoComponent } from './dialog-modificar-trabajo.component';

describe('DialogModificarTrabajoComponent', () => {
  let component: DialogModificarTrabajoComponent;
  let fixture: ComponentFixture<DialogModificarTrabajoComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DialogModificarTrabajoComponent]
    });
    fixture = TestBed.createComponent(DialogModificarTrabajoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
