import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Categoria } from '../common/models/categorias.model';
import { FirestoreService } from '../services/firestore.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dialog-modificar-trabajo',
  templateUrl: './dialog-modificar-trabajo.component.html',
  styleUrls: ['./dialog-modificar-trabajo.component.scss']
})
export class DialogModificarTrabajoComponent  implements OnInit {  

  loading = false;
  Formulario !: FormGroup;
  newUser!: Categoria;


  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
  public dialogRef: MatDialogRef<DialogModificarTrabajoComponent>, 
  private formBuilder: FormBuilder, private firestoreService: FirestoreService, 
  private router:Router){}

  ngOnInit(): void {
    console.log("data",this.data);
    this.Formulario = this.formBuilder.group({
      nombreTrabajo: ['',Validators.required],
    });
    this.Formulario.get('nombreTrabajo')?.setValue(this.data.nombre);
  }

  onNoClick() {
    this.dialogRef.close();
  }

  updateTrabajo(user:Categoria){
    Swal.fire({
      title: '¿Estás seguro que quieres modificar el trabajo?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, modificar trabajo',
      cancelButtonText: 'Cancelar',
    }).then((result) => {
      if (result.isConfirmed) {
            user.nombre = this.Formulario.get('nombreTrabajo')?.value;
        Swal.fire('Agregado!', 'El trabajo ha sido modificado!', 'success').then(
          (result) => {
            this.edit(user);            
          }
        );
      }
    });

  }

  async edit(user: Categoria) {
    this.loading = true;
    console.log('edit -> ', user);
    this.newUser = user;
    await this.firestoreService.updateDocumentID(this.newUser,'categorias', this.newUser.id);
    this.loading = false;
    this.router.navigate(['trabajos']).then(() => {
      window.location.reload();
    });
  }
}
