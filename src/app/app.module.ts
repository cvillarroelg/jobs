import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './materialmodule/materialmodule';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { TrabajosComponent } from './trabajos/trabajos.component';
import { DialogAgregarUsuarioComponent } from './dialog-agregar-usuario/dialog-agregar-usuario.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { DialogModificarUsuarioComponent } from './dialog-modificar-usuario/dialog-modificar-usuario.component';
import { DialogAgregarTrabajoComponent } from './dialog-agregar-trabajo/dialog-agregar-trabajo.component';
// import { AngularFireModule } from '@angular/fire';
import { provideFirebaseApp, initializeApp } from '@angular/fire/app';
import { getFirestore, provideFirestore } from '@angular/fire/firestore';
import { environment } from '../app/environment/environment';
import { DialogModificarTrabajoComponent } from './dialog-modificar-trabajo/dialog-modificar-trabajo.component';
@NgModule({
  declarations: [
    AppComponent,
    UsuariosComponent,
    TrabajosComponent,
    DialogAgregarUsuarioComponent,
    DialogModificarUsuarioComponent,
    DialogAgregarTrabajoComponent,
    DialogModificarTrabajoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [
    provideFirebaseApp(() => initializeApp(environment.firebaseConfig)),
    provideFirestore(()=> getFirestore())
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
