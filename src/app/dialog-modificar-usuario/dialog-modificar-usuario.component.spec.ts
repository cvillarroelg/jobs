import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogModificarUsuarioComponent } from './dialog-modificar-usuario.component';

describe('DialogModificarUsuarioComponent', () => {
  let component: DialogModificarUsuarioComponent;
  let fixture: ComponentFixture<DialogModificarUsuarioComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DialogModificarUsuarioComponent]
    });
    fixture = TestBed.createComponent(DialogModificarUsuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
