import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { UsuarioService } from '../services/usuario.service';

@Component({
  selector: 'app-dialog-modificar-usuario',
  templateUrl: './dialog-modificar-usuario.component.html',
  styleUrls: ['./dialog-modificar-usuario.component.scss']
})
export class DialogModificarUsuarioComponent implements OnInit{

  Formulario !: FormGroup;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
    private formBuilder: FormBuilder, private router: Router, public dialogRef: MatDialogRef<DialogModificarUsuarioComponent>,
    private _usuarioService: UsuarioService) { }

  ngOnInit(): void {
    this.Formulario = this.formBuilder.group({
      nombre: ['',Validators.required],
      apellido: ['',Validators.required],
      celular: ['',Validators.required],
      rut:['',Validators.required],
      direccion:['',Validators.required]
    });
    this.Formulario.get('nombre')?.setValue(this.data.nombre);
    this.Formulario.get('apellido')?.setValue(this.data.apellido);
    this.Formulario.get('celular')?.setValue(this.data.celular);
    this.Formulario.get('rut')?.setValue(this.data.rut);
    this.Formulario.get('direccion')?.setValue(this.data.direccion);
  }

  onNoClick(){
    this.dialogRef.close();
  }

  agregarUsuario(){
    console.log("agregar usuario");
  }

  updateUsuario(){
    let nombre = this.Formulario.get('nombre')?.value;   
    let apellido = this.Formulario.get('apellido')?.value;
    let rut = this.Formulario.get('rut')?.value; 
    let celular = this.Formulario.get('celular')?.value; 
    let direccion = this.Formulario.get('direccion')?.value; 
    let idusuario = this.data.idusuario;

    Swal.fire({
      title: '¿Estás seguro que quieres modificar al usuario?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, modificar usuario',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire(
          'Modificado!',
          'El usuario ha sido modificado',
          'success'
        )
        .then((result) => {
          // console.log("id:", id);
          console.log("nombre:",nombre);
          console.log("apellido:",apellido);
          console.log("rut:",rut);
          console.log("direccion:",direccion);
          console.log("celular:",celular);
          console.log("idusuario:",idusuario);
          this._usuarioService.modificarUsuario(nombre,apellido,rut,direccion,celular,idusuario).subscribe(result => console.log(result));
          // this.categoriaService.updateCategoria(id,nombre).subscribe(result => console.log(result));
          this.router.navigate(['usuarios'])
          .then(() => {
            this.onNoClick();
            window.location.reload();
          });
        });
      }
    })
  }

}
